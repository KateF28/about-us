// Core
import React, {FC} from 'react';
import {AboutState} from "../../context/about/AboutState";
// Components
import {AboutUs} from '../../components';
// Styles
import "../../styles/main.sass";

export const App: FC = () => {
    return (
        <AboutState>
            <div className="container">
                <AboutUs/>
            </div>
        </AboutState>
    );
}
