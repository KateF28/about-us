type ErrorType = null | {
    message: string;
}

// About
type ReviewerType = {
    id: number;
    name: string;
    reviews: number;
    stars: number;
    url: string;
};

type ReviewersType = [] | ReviewerType[];
