export const FETCH_REVIEWERS = 'FETCH_ABOUT';
export const SHOW_LOADER = 'SHOW_LOADER';
export const HIDE_LOADER = 'HIDE_LOADER';
export const SHOW_ERROR = 'SHOW_ERROR';
export const CLEAR_ERROR = 'CLEAR_ERROR';

export interface IAboutState {
    reviewers: ReviewersType;
    loading: Boolean;
    error: ErrorType;
}

export interface IAboutContext extends IAboutState {
    fetchReviewers(): Promise<void>;
}

export type AboutPayloadType =  ReviewersType | ErrorType | undefined;

export type AboutActionType = {
    type: string;
    payload?: ReviewersType | ErrorType;
}
