export class Http {
    // static HEADERS: Headers = {'Content-Type': 'application/json'};

    static async get(url: string) {
        try {
            return await request(url, 'GET');
        } catch (e) {
            throw e;
        }
    }
}

async function request<T>(url: string, method = 'GET', data?: any): Promise<T> {
    const config: {
        method: string;
        // headers: Headers;
        body?: string;
    } = {
        method,
        // headers: Http.HEADERS
    };
    if (method === 'POST' || method === 'PATCH') {
        config.body = JSON.stringify(data);
    }
    const response = await fetch(url, config);
    return await response.json() as Promise<T>;
}

// function api<T>(url: string): Promise<T> {
//     return fetch(url)
//         .then(response => {
//             if (!response.ok) {
//                 throw new Error(response.statusText)
//             }
//             return response.json() as Promise<T>
//         })
// }
