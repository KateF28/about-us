import {
    SHOW_LOADER,
    HIDE_LOADER,
    CLEAR_ERROR,
    SHOW_ERROR,
    FETCH_REVIEWERS,
    IAboutState,
    AboutActionType,
    AboutPayloadType
} from "../types";

const handlers: {[key: string]: (state: IAboutState, payload?: AboutPayloadType) => IAboutState} = {
    [SHOW_LOADER]: (state: IAboutState): IAboutState => ({...state, loading: true}),
    [HIDE_LOADER]: (state: IAboutState): IAboutState => ({...state, loading: false}),
    [CLEAR_ERROR]: (state: IAboutState): IAboutState => ({...state, error: null}),
    [SHOW_ERROR]: (state: IAboutState, payload: AboutPayloadType): IAboutState => {
        return typeof payload === "object" && !Array.isArray(payload) ? {...state, error: payload} : state;
    },
    [FETCH_REVIEWERS]: (state: IAboutState, payload: AboutPayloadType): IAboutState => {
        return Array.isArray(payload) ? {...state, reviewers: payload} : state;
    },
    DEFAULT: (state: IAboutState): IAboutState => state
};

export const aboutReducer = (state: IAboutState, action: AboutActionType): IAboutState => {
    const handler: (state: IAboutState, payload?: AboutPayloadType) => IAboutState = handlers[action.type] || handlers.DEFAULT;
    return action.payload ? handler(state, action.payload): handler(state);
}
