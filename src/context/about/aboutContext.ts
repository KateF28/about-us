import {createContext} from 'react';
import {IAboutContext} from "../types";

export const AboutContext = createContext<Partial<IAboutContext>>({
    reviewers: [],
    loading: false,
    error: null,
    fetchReviewers: async (): Promise<void> => {}
});
