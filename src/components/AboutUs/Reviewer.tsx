// Core
import React, {FC} from 'react';

// Styles
import styles from "./about.module.sass";

type ReviewerPropsType = {
    children?: never;
    reviewer: ReviewerType
}

export const Reviewer: FC<ReviewerPropsType> = ({reviewer}: ReviewerPropsType) => {
    const {name, url, reviews, stars} = reviewer;

    return (
        <div className={styles.reviewer}>
            <img className={styles.reviewer__img} alt={`${name} logo`} src={`/images/${name}_logo.svg`}/>
            <div className={styles.reviewer__stars}>
                <div className={`${styles.reviewer__stars} flex jc-c ai-c`}>
                    <div className={`${styles["reviewer__star-wrapper"]}`}>
                        <p className={`${styles["reviewer__star-score"]}`}
                           style={{width: stars <= 1 ? `${stars*100}%` : "100%"}}>
                            <span className={styles.reviewer__star}><i className="fas fa-star"/></span>
                        </p>
                    </div>
                    <div className={`${styles["reviewer__star-wrapper"]}`}>
                        <p className={`${styles["reviewer__star-score"]}`}
                           style={{width: stars >= 2 ? "100%" : stars > 1 && stars < 2 ? `${+stars.toString().split(".")[1]}%` : 0}}>
                            <span className={styles.reviewer__star}><i className="fas fa-star"/></span>
                        </p>
                    </div>
                    <div className={`${styles["reviewer__star-wrapper"]}`}>
                        <p className={`${styles["reviewer__star-score"]}`}
                           style={{width: stars >= 3 ? "100%" : stars > 2 && stars < 3 ? `${+stars.toString().split(".")[1]}%` : 0}}>
                            <span className={styles.reviewer__star}><i className="fas fa-star"/></span>
                        </p>/
                    </div>
                    <div className={`${styles["reviewer__star-wrapper"]}`}>
                        <p className={`${styles["reviewer__star-score"]}`}
                           style={{width: stars >= 4 ? "100%" : stars > 3 && stars < 4 ? `${+stars.toString().split(".")[1]}%` : 0}}>
                            <span className={styles.reviewer__star}><i className="fas fa-star"/></span>
                        </p>
                    </div>
                    <div className={`${styles["reviewer__star-wrapper"]}`}>
                        <p className={`${styles["reviewer__star-score"]}`}
                           style={{width: stars > 4 && stars < 5 ? `${stars.toString().split(".")[1]}%` : stars >= 5 ? "100%" : 0}}>
                            <span className={styles.reviewer__star}><i className="fas fa-star"/></span>
                        </p>
                    </div>
                </div>
            </div>
            {reviews <= 0 ?
                <p className={styles.reviewer__text}>No reviews</p> :
                <p className={styles.reviewer__text}>
                    {stars} from {reviews} <a className={styles.reviewer__link} href={url} target="_blank"
                                              rel="noopener noreferrer">reviews</a>
                </p>}
        </div>
    );
}
