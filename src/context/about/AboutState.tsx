import React, {useReducer, ReactElement, FC} from 'react';
import {AboutContext} from './aboutContext';
import {aboutReducer} from "./aboutReducer";
import {
    FETCH_REVIEWERS,
    CLEAR_ERROR,
    HIDE_LOADER,
    SHOW_ERROR,
    SHOW_LOADER,
    IAboutState
} from "../types";
import {Http} from "../../http";

type AboutStateType = {
    children: ReactElement;
}

export const AboutState: FC<AboutStateType> = ({children}) => {
    const initialState: IAboutState = {
        reviewers: [],
        loading: false,
        error: null
    };
    const [state, dispatch] = useReducer(aboutReducer, initialState);

    const fetchReviewers = async (): Promise<void> => {
        showLoader();
        clearError();
        try {
            //@ts-ignore
            const data: ReviewersType = await Http.get('https://6076f0d51ed0ae0017d6a1e3.mockapi.io/reviews');
            dispatch({type: FETCH_REVIEWERS, payload: data});
        } catch (e) {
            showError(e.message);
            console.log('Fetch error', e.message);
        } finally {
            hideLoader();
        }
    }

    const showLoader = (): void => dispatch({type: SHOW_LOADER});
    const hideLoader = (): void => dispatch({type: HIDE_LOADER});
    const showError = (error: ErrorType): void => dispatch({type: SHOW_ERROR, payload: error});
    const clearError = (): void => dispatch({type: CLEAR_ERROR});

    return <AboutContext.Provider value={{
        reviewers: state.reviewers,
        loading: state.loading,
        error: state.error,
        fetchReviewers
    }}>{children}</AboutContext.Provider>
}
