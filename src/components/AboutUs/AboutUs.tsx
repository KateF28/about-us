// Core
import React, {FC, useContext, useEffect, useCallback} from 'react';
import {AboutContext} from "../../context/about/aboutContext";

// Components
import {Reviewer} from "./Reviewer";

// Styles
import styles from "./about.module.sass";

type AboutUsType = {
    children?: never;
}

export const AboutUs: FC<AboutUsType> = () => {
    const {reviewers, fetchReviewers, loading} = useContext(AboutContext);
    const loadReviewers = useCallback(async () => fetchReviewers && await fetchReviewers(), [fetchReviewers]);

    useEffect(() => {
        loadReviewers();
        // eslint-disable-next-line
    }, []);

    return (
        <div className={styles.about}>
            <div className={styles.about__wrapper}>
                <div className={styles.about__content}>
                    <h2 className={styles.about__title}>About us</h2>
                    <div className={styles.about__reviewers}>
                        {reviewers && reviewers.length > 0 && (reviewers as Array<ReviewerType>).map((reviewer: ReviewerType) => (
                            <Reviewer key={reviewer.id} reviewer={reviewer}/>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    );
}
